import 'dart:io';

void main() {
  int count = 1;
  String? message =
      'A mobile application, most commonly referred to as an app, is a type of application software designed to run on a mobile device, such as a smartphone or tablet computer. Mobile applications frequently serve to provide users with similar services to those accessed on PCs.';
  var chagetoLow = message.toLowerCase();
  var words = chagetoLow.split(" ");
  List<String> wordsList = [];
  List<int> count_word = [];
  for (int i = 0; i < words.length; i++) {
    var duplicate = false;
    for (int j = 0; j < wordsList.length; j++) {
      if (words[i] == wordsList[j]) {
        duplicate = true;
        count_word[j] = count_word[j] + 1;
        break;
      }
    }
    if (duplicate == false) {
      wordsList.add(words[i]);
      count_word.add(1);
    }
  }
  for (var i = 0; i < wordsList.length; i++) {
    print("${wordsList[i]} ${count_word[i]}");
  }
}
